package fr.univlyon1.m1if.m1if03.resas_api.model;

import jakarta.annotation.Nonnull;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Réservations créées par les utilisateurs.
 * Une réservation comporte :<br>
 * - une date de début et une date de fin<br>
 * - une liste d'IDs de joueurs<br>
 * - une liste de commentaires<br>
 * - un booléen (complète / en recherche de joueurs)<br>
 * Pour la créer, il faut indiquer l'ID de l'utilisateur qui l'a créée.
 */
public class Reservation {
    private static final int MAX_PLAYERS = 2;
    private String courtId;
    private final String ownerId;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private final List<String> players = new ArrayList<>();
    private final List<Comment> comments = new ArrayList<>();

    /**
     * Création d'une réservation.
     * @param courtId Nom du court indiqué dans la réservation
     * @param creatorId Login de l'utilisateur créateur
     * @param startTime Date et heure de début de la réservation
     * @param endTime Date et heure de fin de la réservation
     */
    public Reservation(String courtId, String creatorId, LocalDateTime startTime, LocalDateTime endTime) {
        this.courtId = courtId;
        // Pour permettre de distinguer les instances
        this.startTime = startTime;
        this.endTime = endTime;
        this.ownerId = creatorId;
        this.addPlayer(creatorId);
    }

    public String getOwnerId() {
        return ownerId;
    }

    public String getCourtId() {
        return courtId;
    }

    public void setCourtId(String courtId) {
        this.courtId = courtId;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void addComment(Comment comment) {
        comments.add(comment);
    }

    public List<String> getPlayers() {
        return players;
    }

    public boolean hasPlayer(@Nonnull String player) {
        return players.contains(Objects.requireNonNull(player));
    }

    public boolean addPlayer(String player) {
        if(hasPlayer(player) || isCompleted()) {
            return false;
        }
        return players.add(player);
    }

    public boolean removePlayer(@Nonnull String player) {
        return players.remove(Objects.requireNonNull(player));
    }

    public boolean isCompleted() {
        return this.players.size() >= MAX_PLAYERS;
    }
}
