package fr.univlyon1.m1if.m1if03.resas_api.dto;

/**
 * DTO de type CommentRequest (voir doc OpenAPI).
 * @param content Le contenu du commentaire à ajouter.
 */
public record CommentRequestDto(String content) {
}
