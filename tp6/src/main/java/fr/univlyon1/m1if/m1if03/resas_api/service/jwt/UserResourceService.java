package fr.univlyon1.m1if.m1if03.resas_api.service.jwt;

import fr.univlyon1.m1if.m1if03.resas_api.connection.ConnectionManager;
import fr.univlyon1.m1if.m1if03.resas_api.dto.NamePropertyUserResponseDto;
import fr.univlyon1.m1if.m1if03.resas_api.dto.OwnedReservationsPropertyUserResponseDto;
import fr.univlyon1.m1if.m1if03.resas_api.dto.RegisteredReservationsPropertyUserResponseDto;
import fr.univlyon1.m1if.m1if03.resas_api.dto.UserResponseDto;
import fr.univlyon1.m1if.m1if03.resas_api.dto.UsersResponseDto;
import fr.univlyon1.m1if.m1if03.resas_api.dto.LinkDto;
import fr.univlyon1.m1if.m1if03.resas_api.model.User;
import fr.univlyon1.m1if.m1if03.resas_api.dao.ReservationDao;
import fr.univlyon1.m1if.m1if03.resas_api.dao.UserDao;
import fr.univlyon1.m1if.m1if03.resas_api.util.UrlDecomposer;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.naming.NameAlreadyBoundException;
import javax.naming.NameNotFoundException;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Méthodes de service du contrôleur de ressources sur les utilisateurs.
  */
@Service
public class UserResourceService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private ReservationDao reservationDao;
    @Autowired
    private ConnectionManager connectionManager;

    public List<User> getAllUsers() {
        return userDao.findAll().stream().toList();
    }

    public UsersResponseDto getAllUsersDto() {
        return new UsersResponseDto(userDao
                .findAll().stream()
                .map(User::getLogin)
                .map(s -> "users/" + s)
                .map(LinkDto::new)
                .toList()
        );
    }

    public URI createUser(User user) throws NameAlreadyBoundException {
        userDao.add(user);
        return URI.create("users/" + user.getLogin());
    }

    public User getUser(String login) throws NameNotFoundException {
        return userDao.findOne(login);
    }

    public UserResponseDto getUserDto(String login) throws NameNotFoundException {
        User user = userDao.findOne(login);
        List<LinkDto> ownedReservations = reservationDao
                .findByOwner(login).stream()
                .map(id -> "reservations/" + id)
                .map(LinkDto::new)
                .toList();
        List<LinkDto> playedReservations = reservationDao
                .findByPlayer(login).stream()
                .map(id -> "reservations/" + id)
                .map(LinkDto::new)
                .toList();
        return new UserResponseDto(user.getLogin(), user.getName(), ownedReservations, playedReservations);
    }

    public void updateUser(String login, User user, HttpServletRequest request, HttpServletResponse response) {
        userDao.update(login, user);

        Map<String, Object> props = new HashMap<>();
        props.put("name", user.getName());
        connectionManager.updateUser(request, response, props);
    }

    public void deleteUser(String login) throws NameNotFoundException {
        userDao.deleteById(login);
    }

    public NamePropertyUserResponseDto getUserName(String login) throws NameNotFoundException {
        return new NamePropertyUserResponseDto(userDao.findOne(login).getName());
    }

    public OwnedReservationsPropertyUserResponseDto getReservationsOwnedByUser(String userId) {
        return new OwnedReservationsPropertyUserResponseDto(reservationDao
                .findByOwner(userId).stream()
                .map(id -> "reservations/" + id)
                .map(LinkDto::new)
                .toList()
        );
    }

    public String getReservationOwnedByUserSubProperty(String userId, int reservationIndex, HttpServletRequest request) throws IndexOutOfBoundsException {
        List<Integer> reservations = reservationDao.findByOwner(userId);
        String urlEnd = UrlDecomposer.getUrlEnd(request, 4);
        return reservations.get(reservationIndex) + urlEnd;
    }

    public RegisteredReservationsPropertyUserResponseDto getReservationsPlayedByUser(String userId) {
        return new RegisteredReservationsPropertyUserResponseDto(reservationDao
                .findByPlayer(userId).stream()
                .map(id -> "reservations/" + id)
                .map(LinkDto::new)
                .toList()
        );
    }

    public String getReservationPlayedByUserSubProperty(String userId, int reservationIndex, HttpServletRequest request) throws IndexOutOfBoundsException {
        List<Integer> reservations = reservationDao.findByPlayer(userId);
        String urlEnd = UrlDecomposer.getUrlEnd(request, 4);
        return reservations.get(reservationIndex) + urlEnd;
    }
}
