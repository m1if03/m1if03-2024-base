package fr.univlyon1.m1if.m1if03.resas_api.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import fr.univlyon1.m1if.m1if03.resas_api.model.Comment;
import fr.univlyon1.m1if.m1if03.resas_api.model.Reservation;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * DTO permettant de sérialiser une réservation (voir doc OpenAPI).
 * @param courtId Le court
 * @param ownerId Le propriétaire
 * @param startTime La date et l'heure de début
 * @param endTime La date et l'heure de fin
 * @param players Les utilisateurs inscrits
 * @param comments Les commentaires
 */
@JacksonXmlRootElement(localName = "reservation")
public record ReservationResponseDto(
        String courtId,
        String ownerId,
        @JsonFormat(
                shape = JsonFormat.Shape.STRING,
                pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        ZonedDateTime startTime,
        @JsonFormat(
                shape = JsonFormat.Shape.STRING,
                pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        ZonedDateTime endTime,
        @JacksonXmlElementWrapper(localName = "players")
        List<LinkDto> players,
        @JacksonXmlElementWrapper(localName = "comments")
        List<Comment> comments) {
    /**
     * @param courtId   Le court réservé
     * @param ownerId   Le propriétaire de la réservation
     * @param startTime Date et heure de début
     * @param endTime   Date et heure de fin
     * @param players   Liste des utilisateurs inscrits dans la réservation
     * @param comments  Liste de commentaires de la réservation
     */
    public ReservationResponseDto {
    }

    public static ReservationResponseDto of(Reservation reservation) {
        return new ReservationResponseDto(
                reservation.getCourtId(),
                reservation.getOwnerId(),
                ZonedDateTime.of(reservation.getStartTime(), ZoneId.systemDefault()),
                ZonedDateTime.of(reservation.getEndTime(), ZoneId.systemDefault()),
                reservation.getPlayers().stream().map(p -> "users/" + p).map(LinkDto::new).toList(),
                reservation.getComments()
        );
    }
}
