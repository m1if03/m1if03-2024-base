package fr.univlyon1.m1if.m1if03.resas_api.service.jwt;

import fr.univlyon1.m1if.m1if03.resas_api.dao.ReservationDao;
import fr.univlyon1.m1if.m1if03.resas_api.model.User;
import fr.univlyon1.m1if.m1if03.resas_api.dao.UserDao;
import fr.univlyon1.m1if.m1if03.resas_api.connection.ConnectionManager;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.naming.NameNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Méthodes de service du contrôleur d'opérations sur les utilisateurs.
  */
@Service
public class UserOperationService {

    @Autowired
    private ConnectionManager connectionManager;
    @Autowired
    private UserDao userDao;
    @Autowired
    private ReservationDao reservationDao;

    /**
     * Méthode réalisant le login : valide le contenu de la requête et place les informations sur l'utilisateur dans une Map en attribut de requête.
     * @param user L'utilisateur trouvé dans le DAO
     * @param request La requête à laquelle la méthode va rajouter un attribut "properties" dans lequel seront les listes owned et played reservations.
     * @param response La réponse (nécessaire pour le JwtConnectionManager qui y rajoute le header "Authorization" avec le token JWT).
     * @throws NameNotFoundException Si le login de l'utilisateur ne correspond pas à un utilisateur existant
     * @throws MatchException Si la vérification des credentials de l'utilisateur a échoué.
     */
    public void login(User user, HttpServletRequest request, HttpServletResponse response) throws NameNotFoundException, MatchException {
        User userFromDao = userDao.findOne(user.getLogin());
        // Vérification des credentials : on se sert du nom comme d'un mot de passe. Bof...
        if(!user.getName().equals(userFromDao.getName())) {
            throw new MatchException("Le nom doit correspondre à celui dans le DAO.", null);
        }

        // Récupération des propriétés de l'utilisateur
        List<Integer> ownedReservations = reservationDao.findByOwner(user.getLogin());
        List<Integer> playedReservations = reservationDao.findByPlayer(user.getLogin());

        // Ajout des réservations possédées et auxquelles il participe pour les retrouver plus facilement
        Map<String, Object> props = new HashMap<>();
        if(!ownedReservations.isEmpty()) {
            props.put("own", ownedReservations);
        }
        if(!playedReservations.isEmpty()) {
            props.put("ply", playedReservations);
        }

        // Passage des propriétés au ConnectionManager par un attribut de requête
        request.setAttribute("properties", props);
        connectionManager.connect(request, response, userFromDao);
    }

    /**
     * Méthode appelant la déconnexion dans le <code>ConnectionManager</code>
     * @param request Inutilisée en JWT
     */
    public void logout(HttpServletRequest request) {
        connectionManager.disconnect(request);
    }
}
