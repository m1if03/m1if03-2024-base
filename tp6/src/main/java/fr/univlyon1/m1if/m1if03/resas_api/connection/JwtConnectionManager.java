package fr.univlyon1.m1if.m1if03.resas_api.connection;

import fr.univlyon1.m1if.m1if03.resas_api.model.User;
import fr.univlyon1.m1if.m1if03.resas_api.util.ResasJwtTokenProvider;
import io.jsonwebtoken.Claims;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Gestion de la connexion par token JWT.
 */
@Service
public class JwtConnectionManager implements ConnectionManager {

    @Autowired
    private ResasJwtTokenProvider resasJwtTokenProvider;

    @Override
    @SuppressWarnings("unchecked")
    public void connect(HttpServletRequest request, HttpServletResponse response, User user) {
        String jwt;
        Map<String, Object> claims = (Map<String, Object>) request.getAttribute("properties");
        if(claims == null || claims.isEmpty()) {
            jwt = resasJwtTokenProvider.generateToken(user);
        } else {
            claims.put("sub", user.getLogin());
            claims.put("name", user.getName());
            jwt = resasJwtTokenProvider.generateToken(claims);
        }
        response.setHeader("Authorization", "Bearer " + jwt);
    }

    /**
     * Ne fait rien.
     * En toute logique, il faudrait se souvenir des utilisateurs déconnectés
     * pour qu'ils ne puissent pas renvoyer un token valide après déconnexion et être authentifiés quand-même.
     * @param request Inutilisée en JWT
     */
    @Override
    public void disconnect(HttpServletRequest request) {
        // The logout logic is often managed client-side, by simply removing the token.
    }

    @Override
    public boolean isConnected(HttpServletRequest request) {
        String jwt = request.getHeader("Authorization");
        if(jwt != null && jwt.startsWith("Bearer ")) {
            return resasJwtTokenProvider.validateToken(jwt.substring(7));
        }
        return false;
    }

    @Override
    public String getUser(HttpServletRequest request) {
        String jwt = request.getHeader("Authorization");
        if(jwt != null && jwt.startsWith("Bearer ")) {
            Claims claims = resasJwtTokenProvider.getClaimsFromToken(jwt.substring(7));
            return claims.getSubject();
        }
        return null;
    }

    @Override
    public void updateUser(HttpServletRequest request, HttpServletResponse response, Map<String, Object> properties) {
        String jwt = request.getHeader("Authorization");
        if(jwt != null && jwt.startsWith("Bearer ")) {
            Claims claims = resasJwtTokenProvider.getClaimsFromToken(jwt.substring(7));
            // On crée une nouvelle map de propriétés, car les claims sont immutables et la map en paramètre peut l'être aussi.
            Map<String, Object> newProps = new HashMap<>(claims);
            properties.forEach(
                    (key, value) -> {
                        if(value != null) {
                            newProps.merge(key, value,
                                    (oldValue, newValue) -> newValue);
                        }
                    }
            );
            jwt = resasJwtTokenProvider.generateToken(newProps);
            response.setHeader("Authorization", "Bearer " + jwt);
        }
    }

    @Override
    public Object getUserProperty(HttpServletRequest request, String key) {
        String jwt = request.getHeader("Authorization");
        if(jwt != null && jwt.startsWith("Bearer ")) {
            Claims claims = resasJwtTokenProvider.getClaimsFromToken(jwt.substring(7));
            return claims.get(key);
        }
        return null;
    }
}
