# Scénario de la démonstration

## Application à présenter

Vous ferez la démonstration sur votre application (SPA) du TP7, qui interroge votre API du TP6.

- votre application doit être déployée sur votre VM et accessible à la racine du serveur nginx, en HTTPS
- votre API doit être déployée sur votre VM et accessible sur Tomcat, à travers le proxy nginx

Si un ou plusieurs de ces élémenst (SPA, API, déploiements) ne fonctionnent pas correctement, vous pouvez faire une présentation "en mode dégradé" : déploiements sur le Tomcat de votre VM (port 8080), déploiements en local sur votre PC, application côté serveur en Spring (TP4) ou application côté serveur en servlets + JSP (TP2). Bien entendu, la note de la démo sera d'autant plus impactée par ce mode dégradé que vous vous éloignerez de la configuration demandée.

## Horaire et préparation

Les horaires des démos de chaque binôme seront affichés sur Tomuss. Vous aurez 5 minutes de présentation par groupe, plus éventuellement, quelques minutes de questions.

Merci de faire en sorte d'être prêts un peu avant l'heure, car il peut arriver qu'un groupe avant vous ne le soit pas. Dans ce cas, je passe au suivant pour ne pas perdre de temps.

## Scénario

Le scénario reprend à peu près celui des requêtes Postman (moins la création/destruction des utilisateurs).

**Il est demandé d'arriver à la démo avec deux navigateurs/onglets avec 2 utilisateurs différents connectés** : user 1 et user 2. Sur chaque page, il est demandé d'ouvrir les devtools, sur l'onglet "réseau".

- user 1 :
  - visualiser la liste des réservations
  - créer une réservation
  - modifier cette réservation
  - voir son profil -> la réservation doit apparaître

- user 2 :
  - s'inscrire à la réservation
  - commenter la réservation

- user 1 :
  - visualiser les modifications faites à la réservation par user 2
  - voir son profil -> 304
  - afficher la réservation
  - se désinscrire de la réservation

- user 2 :
  - afficher la réservation -> visualiser les modifications faites par user 1
  - essayer de supprimer la réservation -> échec
  - afficher la réservation -> 304

- user 1 :
  - supprimer la réservation


