/**
 * Fonction spécifiques au métier de l'application.
 * À compléter en vous inspirant de ce qui est déjà fait.
 * La partie ("region") Modèle fait des transformations de données entre les objets de l'application et les contenus des requêtes/réponses.
 * La region Requêtes asynchrones envoie les requêtes au serveur et traite les réponses.
 * @Author Lionel Médini
 */

//#region Modèle
/**
 * Génère un objet correspondant à une requête de création de réservation.
 * @returns {{courtId, creatorId, startTime: string, endTime: string}} Correspond à un ReservationRequestDto côté serveur
 */
function getReservationRequestDto() {
    // Formatage des dates à partir des données du formulaire
    let start = new Date(document.getElementById("startDate").value);
    start.setHours(Number(document.getElementById("startTime").value.split(":")[0]));
    start.setMinutes(Number(document.getElementById("startTime").value.split(":")[1]));
    const end = new Date(start.setMilliseconds(start.getMilliseconds() + Number(document.getElementById("duration").value) * 60000));

    return {
        courtId: document.getElementById("courtId").value,
        creatorId: localUser.login,
        startTime: start.toISOString(),
        endTime: end.toISOString()
    };
}

/**
 * Renvoie le résumé d'une réservation prêt à être templaté dans les vues de type liste
 * @param reservationResponse Le résultat de la promesse "response.json()" après récupération d'une réservation
 * @param resaId L'ID de la réservation (qui n'est pas dans le JSON de la réponse)
 * @returns {{resaId: *, courtId: *, ownerId, startDate: *, startTime}} Le résumé de la réservation
 */
function getReservationResume(reservationResponse, resaId) {
    return {
        resaId: resaId.replace("reservations/", "reservation/"),
        courtId: reservationResponse.courtId,
        ownerId: reservationResponse.ownerId,
        startDate: getFormattedDate(new Date(reservationResponse.startTime)),
        startTime: getFormattedTime(new Date(reservationResponse.startTime))
    };
}
//#endregion Modèle

//#region Requêtes asynchrones
/**
 * Fonction à réaliser pour récupérer les données des listes de réservations que l'utilisateur connecté organise ou auxquelles il est inscrit.
 */
function loadIndex() {}

/**
 * Met à jour le nombre d'utilisateurs de l'API sur la vue "index".
 */
function loadUserCount() {
    sendRequest("/users", "GET", null, false)
        .then((response) => {
            if(response.ok && response.headers.get("Content-Type").includes("application/json")) {
                return response.json();
            } else {
                throw new Error("Response is error (" + response.status + ") or does not contain JSON (" + response.headers.get("Content-Type") + ").");
            }
        }).then((json) => {
        if(Array.isArray(json.users)) {
            document.getElementById("nbUsers").innerText = json.users.length;
        } else {
            throw new Error(json.users + " is not an array.");
        }
    }).catch((err) => {
        console.error("In loadUserCount: " + err);
    });
}

/**
 * Charge un utilisateur et template la vue "user" avec cet utilisateur.
 * @param login Le login de l'utilisateur à afficher ou null/undefined pour afficher l'utilisateur logué
 */
function loadUser(login) {
    if(login) {
        sendRequest("/users/" + login, "GET")
            .then((response) => {
                if (response.ok && response.headers.get("Content-Type").includes("application/json")) {
                    response.json().then((user) => {
//                        console.log("In loadUser:", user);
                        templateUser(user);
                    });
                } else {
                    throw new Error("Bad response code (" + response.status + ").");
                }
            })
            .catch((err) => {
                displayRequestResult("Erreur de récupération de l'utilisateur " + login, "alert-danger");
                console.error("In loadUser: " + err);
            });
    } else {
        sendRequest("/users/" + localUser.login, "GET")
            .then((response) => (response.json()).then((user) => {
                templateUser(user);
            }));
    }
}

/**
 * Charge la liste de réservations et template la vue "reservationList" avec cette liste.
 */
function loadReservationList() {
    sendRequest("/reservations", "GET")
        .then((response) => {
            if (response.ok && response.headers.get("Content-Type").includes("application/json")) {
                response.json().then((responseBody) => {
                    Promise.all(responseBody.reservations.map((reservationLink) => {
                        return sendRequest("/" + reservationLink.link, "GET")
                            .then((response) => {
                                if(response.ok || response.status === 304) {
                                    return response.json().then((reservation) => {
                                        return getReservationResume(reservation, reservationLink.link);
                                    });
                                }
                            })
                            .catch((err) => {
                                console.error("In loadReservationList: " + err);
                            });
                    }))
                        .then((reservationResumes) => {
//                            console.log("In loadReservationList:", reservationResumes);
                            templateReservations({resumes: reservationResumes});
                        });
                });
            } else {
                throw new Error("Bad response code (" + response.status + ").");
            }
        })
        .catch((err) => {
            displayRequestResult("Erreur de récupération de la liste de réservations.", "alert-danger");
            console.error("In loadReservationList: " + err);
        });
}

/**
 * Charge une réservation et template la vue "reservation" avec cette réservation.
 * @param resaId L'ID de la réservation à templater
 */
function loadReservation(resaId) {
    sendRequest("/reservations/" + resaId, "GET")
        .then((response) => {
            if (response.ok && response.headers.get("Content-Type").includes("application/json")) {
                response.json().then((reservation) => {
//                    console.log("In loadReservation:", reservation);
                    templateReservation(reservation);
                });
            } else {
                throw new Error("Bad response code (" + response.status + ").");
            }
        })
        .catch((err) => {
            displayRequestResult("Erreur de récupération de la réservation " + resaId, "alert-danger");
            console.error("In loadReservation: " + err);
        });
}

/**
 * Crée une nouvelle réservation à partir des données dans le formulaire de la page.
 */
function createReservation() {
    // Récupération des données correctement formatées
    const body = getReservationRequestDto();
//    console.log("In createReservation:", body);

    // Envoi de la requête
    sendRequest("/reservations", "POST", body)
        .then((response) => {
            if(response.status === 201 && response.headers.get("Location").includes("reservations/")) {
                const resaId = response.headers.get("Location").split("reservations/")[1];
                displayRequestResult("Réservation créée", "alert-success");
            } else {
                throw new Error("Bad response code (" + response.status + ") or missing header.");
            }
        })
        .catch((err) => {
            displayRequestResult("Création de la réservation refusée ou impossible", "alert-danger");
            console.error("In createReservation: " + err);
        })
}

/**
 * Envoie la requête de login en fonction du contenu des champs de l'interface.
 */
function connect() {
    const body = {
        login: document.getElementById("login_input").value,
        name: document.getElementById("name_input").value
    };
    sendRequest("/users/login", "POST", body)
        .then((response) => {
            if(response.status === 204) {
                displayRequestResult("Connexion réussie", "alert-success");
                token = response.headers.get("Authorization").substring(7);
                console.log("In login: Authorization = " + token);
                location.hash = "#index";
                displayConnected(true);
                localUser = body;
/*
                sendRequest("/users/" + body.login, "GET")
                    .then((response) => (response.json()).then((user) => {
                        localUser = user;
//                        console.log("Login\nlocalUser :" + JSON.stringify(localUser));
                    }));
*/
            } else {
                throw new Error("Bad response code (" + response.status + ").");
            }
        })
        .catch((err) => {
            displayRequestResult("Connexion refusée ou impossible", "alert-danger");
            console.error("In login: " + err);
        })
}

/**
 * Réalise la déconnexion.
 */
function deco() {
    sendRequest("/users/logout", "POST")
        .then((response) => {
            if(response.status === 204) {
                displayRequestResult("Déconnexion réussie", "alert-success");
                token = null;
                delete(localUser);
//                console.log("Logout\nlocalUser :" + JSON.stringify(localUser));
                location.hash = "#index";
            } else {
                throw new Error("Bad response code (" + response.status + ").");
            }
        })
        .catch((err) => {
            displayRequestResult("Déconnexion refusée ou impossible", "alert-danger");
            console.error("In logout: " + err);
        })

    location.hash = "#index";
    displayConnected(false);
}

/**
 * Convenience function qui génère et envoie la requête de la "bonne" façon pour l'application.
 * @param path Le chemin sur le serveur à partir de l'URL de base
 * @param method La méthode HTTP
 * @param body Un objet JS sérialisable en JSON ou null
 * @param auth Un booléen qui indique si un token doit être envoyé (si utilisateur connecté)
 * @returns {Promise<Response>} La promesse renvoyée par fetch après l'envoi de la requête
 */
function sendRequest(path, method, body = null, auth = true) {
    let requestConfig = {
        method: method,
        headers: new Headers(),
        mode: "cors" // pour le cas où vous utilisez un serveur différent pour l'API et le client.
    };

    if(body != null) {
        requestConfig.headers.append("Content-Type", "application/json");
        requestConfig.body = JSON.stringify(body);
    } else {
        requestConfig.headers.append("Accept", "application/json");
    }
    if(auth && token != null) {
        requestConfig.headers.append("Authorization", "Bearer " + token);
    }

    return fetch(baseUrl + path, requestConfig);
}

/**
 * Mise à jour de la page d'accueil
 */
setInterval(loadUserCount, 5000);
//#endregion Requêtes asynchrones
