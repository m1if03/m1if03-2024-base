/**
 * Scripts exécutés côté client pour rendre l'application fonctionnelle.
 * A priori, vous n'avez pas à les modifier.
 * @Author Lionel Médini
 */

//#region Routage
/**
 * Mécanisme de routage de la SPA.
 * Contient la "configuration" de l'application.
 */
window.addEventListener('hashchange', () => {
    // On récupère l'ID de la vue
    const newView = location.hash.split("/")[0];
//    console.log(newView);

    // On appelle une fonction de callback (si nécessaire)
    switch (newView) {
        case "#index":
            loadIndex();
            break;
        case "#user":
            loadUser(location.hash.split("/")[1]);
            break;
        case "#reservation":
            loadReservation(location.hash.split("/")[1]);
            break;
        case "#reservationList":
            loadReservationList();
            break;
    }

    // On affiche la vue correspondante (ou une erreur)
    if(document.querySelector(newView)) {
        // On affiche la vue
        show(newView);
    } else {
        show("#error");
    }
});

/**
 * Fait basculer la visibilité des éléments affichés quand le hash change.<br>
 * Passe l'élément actif en inactif et l'élément correspondant au hash en actif.
 * @param hash une chaîne de caractères (trouvée a priori dans le hash) contenant un sélecteur CSS indiquant un élément à rendre visible.
 */
function show(hash) {
    const oldActiveElement = document.querySelector(".active");
    oldActiveElement.classList.remove("active");
    oldActiveElement.classList.add("inactive");
    const newActiveElement = document.querySelector(hash);
    newActiveElement.classList.remove("inactive");
    newActiveElement.classList.add("active");
}
//#endregion Routage

//#region Variables applicatives
/**
 * Token de connexion. Enregistré lors de la connexion.
 * Mis à jour dès que l'une des réponses en contient un nouveau.
 * Mis à null lors de la déconnexion. Ne pas le renvoyer s'il est null.
 */
let token = null;

/**
 * Mémorisation de l'utilisateur connecté.
 */
let localUser;
//#endregion Variables applicatives

//#region Affichage
/**
 * Affiche pendant 5 secondes un message sur l'interface indiquant le résultat de la dernière opération.
 * @param text Le texte du message à afficher
 * @param cssClass La classe CSS dans laquelle afficher le message (défaut = alert-info)
 */
function displayRequestResult(text, cssClass = "alert-info") {
    const requestResultElement = document.getElementById("requestResult");
    requestResultElement.innerText = text;
    requestResultElement.classList.add(cssClass);
    setTimeout(
        () => {
            requestResultElement.classList.remove(cssClass);
            requestResultElement.innerText = "";
        }, 5000);
}

/**
 * Affiche ou cache les éléments de l'interface qui nécessitent une connexion.
 * @param isConnected un Booléen qui dit si l'utilisateur est connecté ou pas
 */
function displayConnected(isConnected) {
    const elementsRequiringConnection = document.getElementsByClassName("requiresConnection");
    let visibilityValue = isConnected ? "block" : "none";
    for(const element of elementsRequiringConnection) {
        element.style.display = visibilityValue;
    }
    const elementsRequiringDisconnection = document.getElementsByClassName("requiresDisconnection");
    visibilityValue = !isConnected ? "block" : "none";
    for(const element of elementsRequiringDisconnection) {
        element.style.display = visibilityValue;
    }
}

/**
 * Renvoie le format d'affichage attendu pour la date
 * @param timeStamp Le timestamp dont on veut la date
 * @returns {string} La date "simplement" formatée
 */
function getFormattedDate(timeStamp) {
    const formattedDate = new Intl.DateTimeFormat("fr-FR", {
        weekday: "long",
        year: "numeric",
        month: "long",
        day: "numeric",
    }).format(timeStamp);
    return formattedDate.charAt(0).toUpperCase() + formattedDate.slice(1);
}

/**
 * Renvoie le format d'affichage attendu pour l'heure
 * @param timeStamp Le timestamp dont on veut l'heure
 * @returns {string} L'heure "simplement" formatée (heure et minutes)
 */
function getFormattedTime(timeStamp) {
    return timeStamp.toLocaleTimeString().substring(0, 5);
}
//#endregion Affichage

//#region Templating
/**
 * Exemple d'utilisation de la fonction de templating pour un utilisateur.
 * @param user Les données reçues du serveur représentant l'utilisateur à templater
 */
function templateUser(user) {
    return template(user, 'userTemplate', 'user');
}

/**
 * Exemple d'utilisation de la fonction de templating pour un utilisateur.
 * @param reservation Les données reçues du serveur représentant l'utilisateur à templater
 */
function templateReservation(reservation) {
    return template(reservation, 'reservationTemplate', 'reservation');
}

/**
 * Exemple d'utilisation de la fonction de templating pour un utilisateur.
 * @param reservations Les données reçues du serveur représentant l'utilisateur à templater
 */
function templateReservations(reservations) {
    return template(reservations, 'reservationListTemplate', 'reservationList');
}

/**
 * Réalise le templating par application d'un template Mustache
 * @param data le modèle à templater
 * @param templateId l'ID de l'élément contenant le template
 * @param targetId l'ID de la vue (élément où placer le résultat)
 */
function template(data, templateId, targetId) {
    const template = document.getElementById(templateId).innerHTML;
//    document.getElementById(targetId).innerHTML = Mustache.render(template, data, {resume: document.getElementById("reservationResumeTemplate").innerText});
}
//#endregion Templating
